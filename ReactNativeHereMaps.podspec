Pod::Spec.new do |s|
    s.name         = "ReactNativeHereMaps"
    s.version      = "1.0.0"
    s.summary      = "Here xcframework for react native wrapper"
    s.description  = <<-DESC
    Here SDK explore edition xcframework for react native wrapper
    DESC
    s.homepage     = "https://gitlab.com/ajayg9626"
    s.license      = "MIT"
    s.author       = { "Ajay G" => "ajay@zeliot.in" }
    s.source       = { :git => "https://gitlab.com/ajayg9626/reactnativeheremapswrapper.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "heresdk.xcframework"
    s.platform = :ios, "11.0"
    s.swift_version = "5.0"
end